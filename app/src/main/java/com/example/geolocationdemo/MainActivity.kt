package com.example.geolocationdemo

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.core.location.LocationManagerCompat.requestLocationUpdates
import com.google.android.gms.location.*
import com.karumi.dexter.PermissionToken

import com.karumi.dexter.MultiplePermissionsReport

import com.karumi.dexter.listener.multi.MultiplePermissionsListener

import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.PermissionRequest


class MainActivity : AppCompatActivity() {
    var btn_get_location: Button? = null
    var btn_stop_location: Button? = null
    var txt_latitude: TextView? = null
    var txt_longitude: TextView? = null

    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var locationRequest: LocationRequest
    lateinit var locationCallback: LocationCallback
    lateinit var location: Location
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_get_location = findViewById(R.id.btn_get_location)
        btn_stop_location = findViewById(R.id.btn_stop_location)
        txt_latitude = findViewById(R.id.txt_latitude)
        txt_longitude = findViewById(R.id.txt_longitude)

        btn_get_location?.setOnClickListener {
            permission()
        }

        btn_stop_location?.setOnClickListener {
            stop()
        }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.create()
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        locationRequest.setInterval(2000)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)
                Log.i("Varun_tag", "Location is result is available")

            }

            override fun onLocationAvailability(p0: LocationAvailability) {
                super.onLocationAvailability(p0)
                if (p0.isLocationAvailable) {
                    Log.i("Varun_tag", "Location is availabel")
                } else {
                    Log.i("Varun_tag", "Location is not availabel")
                }
            }
        }
    }

    private fun permission() {
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
            ).withListener(object : MultiplePermissionsListener {
                @SuppressLint("MissingPermission")
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    fusedLocationProviderClient.requestLocationUpdates(
                        locationRequest,
                        locationCallback,
                        Looper.getMainLooper()
                    )
                    fusedLocationProviderClient.lastLocation.addOnSuccessListener {
                        location = it
                        txt_latitude?.setText(location.latitude.toString())
                        txt_longitude?.setText(location.longitude.toString())
                    }

                    fusedLocationProviderClient.lastLocation.addOnFailureListener {
                        Log.i("Varun_tag", "Exception" + it.message)
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) {

                }
            }).check()

    }

    @SuppressLint("MissingPermission")
    private fun stop() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)


    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permission()
    }

    override fun onDestroy() {
        super.onDestroy()
        stop()
    }
}