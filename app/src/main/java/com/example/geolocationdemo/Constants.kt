package com.example.geolocationdemo

interface Constants {
    companion object{
        var success_result: Int
            get() = 0
            set(value) = TODO()
        var failure_result: Int
            get() = 1
            set(value) = TODO()
        var package_name: String
            get() = "com.example.geolocationdemo"
            set(value) = TODO()
        var receiver: String
            get() = package_name + ".RECEIVER"
            set(value) = TODO()


        var result_data_key: String
            get() = package_name + ".RESULT_DATA_KEY"
            set(value) = TODO()


        var location_data_extra: String
            get() = package_name + ".LOCATION_DATA_EXTRA"
            set(value) = TODO()


    }




}