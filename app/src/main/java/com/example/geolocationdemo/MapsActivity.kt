package com.example.geolocationdemo

import android.Manifest
import android.annotation.SuppressLint
import android.location.Geocoder
import android.location.Location
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.widget.Toast

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.geolocationdemo.databinding.ActivityMapsBinding
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

class MapsActivity(handler: Handler?) : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var locationRequest: LocationRequest
    lateinit var locationCallback: LocationCallback
    lateinit var location: Location
    lateinit var addressResultReceiver: AddressResultReceiver
    var locationAddress = ""
    var isAddressReq = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        var bundle = null
//        if (savedInstanceState == null) {
//            bundle =
//                savedInstanceState?.getBundle(resources.getString(R.string.google_maps_key)) as Nothing?
//        }
//
//        binding.mapView.onCreate(bundle)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.create()
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        locationRequest.setInterval(2000)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)
                Log.i("Varun_tag", "Location is result is available")
            }

            override fun onLocationAvailability(p0: LocationAvailability) {
                super.onLocationAvailability(p0)
                if (p0.isLocationAvailable) {
                    Log.i("Varun_tag", "Location is availabel")
                } else {
                    Log.i("Varun_tag", "Location is not availabel")
                }
            }
        }
        permission()

        binding.btnGetAddress.setOnClickListener {
            getAddress(location)
        }
    }

    private fun getAddress(location: Location) {
        if (!Geocoder.isPresent()) {
            Toast.makeText(this, "Geo coder is not present", Toast.LENGTH_SHORT).show()
        } else {
            startAddressFetchService()
        }
    }

    private fun startAddressFetchService() {


    }

    override fun onMapReady(googleMap: GoogleMap) {

        mMap = googleMap
        var latlng = LatLng(location.latitude, location.longitude)
        mMap.addMarker(MarkerOptions().position(latlng).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latlng))
    }


    private fun permission() {
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
            ).withListener(object : MultiplePermissionsListener {
                @SuppressLint("MissingPermission")
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    fusedLocationProviderClient.requestLocationUpdates(
                        locationRequest,
                        locationCallback,
                        Looper.getMainLooper()
                    )
                    fusedLocationProviderClient.lastLocation.addOnSuccessListener {
                        location = it
                        //with fragment
                        val mapFragment = supportFragmentManager
                            .findFragmentById(R.id.map) as SupportMapFragment
                        mapFragment.getMapAsync(this@MapsActivity)

                        //with map view
//                        binding.mapView.getMapAsync(this@MapsActivity)

                    }

                    fusedLocationProviderClient.lastLocation.addOnFailureListener {
                        Log.i("Varun_tag", "Exception" + it.message)
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) {

                }
            }).check()

    }

    @SuppressLint("MissingPermission")
    private fun stop() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)


    }

//    override fun onStart() {
//        super.onStart()
//        binding.mapView.onStart()
//    }
//
//    override fun onResume() {
//        super.onResume()
//        binding.mapView.onResume()
//    }
//
//    override fun onStop() {
//        super.onStop()
//        binding.mapView.onStop()
//    }
//
//    override fun onPause() {
//        super.onPause()
//        binding.mapView.onPause()
//    }


//    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
//        super.onSaveInstanceState(outState, outPersistentState)
//        var bundle = outState.getBundle(resources.getString(R.string.google_maps_key))
//        if (bundle == null) {
//            bundle = Bundle()
//            outState.putBundle(resources.getString(R.string.google_maps_key), bundle)
//        }
//        binding.mapView.onSaveInstanceState(bundle)
//    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permission()
    }

    override fun onDestroy() {
        super.onDestroy()
//        binding.mapView.onDestroy()
        stop()
    }

    inner class AddressResultReceiver(handler: Handler?) : ResultReceiver(handler) {
        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
            super.onReceiveResult(resultCode, resultData)
            if (resultCode == Constants.success_result) {
                locationAddress = resultData?.getString(Constants.result_data_key).toString()
                binding.txtAddress.text = locationAddress
                isAddressReq = false
                binding.txtAddress.setTextColor(resources.getColor(R.color.black))
            } else {
                locationAddress = resultData?.getString(Constants.result_data_key).toString()
                binding.txtAddress.text = locationAddress
                binding.txtAddress.setTextColor(resources.getColor(R.color.teal_700))
            }
        }

    }
}